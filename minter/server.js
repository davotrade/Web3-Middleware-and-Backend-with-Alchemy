const express = require('express');
const cors = require('cors');
const { execute } = require('./executeCallAlchemy');

const app = express();

// Use CORS middleware
app.use(cors());

app.post('/triggerScript', async (req, res) => {
  try {
    await execute();
    res.status(200).send('PET tokens minted successfully!');
  } catch (error) {
    res.status(500).send('Error minting PET tokens!');
  }
});

const PORT = process.env.PORT || 4000;
app.listen(PORT, () => {
  console.log(`Server running on port ${PORT}`);
});
